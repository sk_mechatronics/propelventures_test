﻿using System;
using System.Threading.Tasks;

namespace business.api
{
    public interface IPropelClient : IDisposable
    {
        Task SendData(string customerID, decimal purchasesTotal, decimal purchasesGstTotal,
            decimal invoicesTotal, decimal invoicesGstTotal);
    }
}
﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace business.api
{
    public class PropelApiClient : IPropelClient, IDisposable
    {
        private readonly HttpClient client;

        public PropelApiClient()
        {
            var baseServiceEndpoint = ConfigurationManager.AppSettings["BaseServiceEndpoint"];
            client = new HttpClient { BaseAddress = new Uri(baseServiceEndpoint) };
        }

        public void Dispose()
        {
            client.Dispose();
        }

        public async Task SendData(string customerID, decimal purchasesTotal, decimal purchasesGstTotal,
            decimal invoicesTotal, decimal invoicesGstTotal)
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "0vr67gh38dj3djsh59dkp32");

            // Replace with Newtonsoft's formatter later on.
            var response = await client.PostAsync("/api/receiveGstData", new StringContent($"{{ \"customerId\" : \"{customerID}\", \"purchases\" : {{ \"total\" : \"{purchasesTotal}\", \"gst\" : \"{purchasesGstTotal}\" }}, \"invoices\" : {{ \"total\" : \"{invoicesTotal}\", \"gst\" : \"{invoicesGstTotal}\" }} }}", Encoding.UTF8, "application/json"));
        }

    }
}

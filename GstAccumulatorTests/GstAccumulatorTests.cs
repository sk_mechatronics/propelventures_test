﻿using business.framework;
using FluentAssertions;
using NUnit.Framework;
using System;

namespace GstAccumulatorTests
{
    public class GstAccumulatorTests
    {
        [Test]
        public void When_calculating_a_gst_with_no_new_gst()
        {
            var gstAccumulator = new GstAccumulator(0.1m, 0, null);
            gstAccumulator.Accumulate(100, DateTime.UtcNow);
            gstAccumulator.Accumulate(150, DateTime.UtcNow);

            gstAccumulator.Total.Should().Be(275.00m);
            gstAccumulator.TotalGst.Should().Be(25.00m);
        }

        [Test]
        public void When_calculating_a_gst_with_new_gst_before_effective_date()
        {
            // Setting original gst to zero to exclude effects.
            // Date is tomorrow

            var gstAccumulator = new GstAccumulator(0.0m, 0.5m, DateTime.UtcNow.AddDays(1));
            gstAccumulator.Accumulate(100, DateTime.UtcNow);
            gstAccumulator.Accumulate(200, DateTime.UtcNow);

            gstAccumulator.Total.Should().Be(300.00m);
            gstAccumulator.TotalGst.Should().Be(0.00m);
        }

        [Test]
        public void When_calculating_a_gst_with_new_gst_at_and_after_effective_date()
        {
            // Setting original gst to zero to exclude effects.
            // Date is tomorrow

            var todaysDate = DateTime.UtcNow;
            var midnightToday = new DateTime(todaysDate.Year, todaysDate.Month, todaysDate.Day, 0, 0, 0);

            var gstAccumulator = new GstAccumulator(0.0m, 0.5m, midnightToday);
            gstAccumulator.Accumulate(100, midnightToday);
            gstAccumulator.Accumulate(200, midnightToday.AddHours(2));

            gstAccumulator.Total.Should().Be(450.00m);
            gstAccumulator.TotalGst.Should().Be(150.00m);
        }

        [Test]
        public void When_calculating_a_gst_with_new_gst_before_and_after_effective_date()
        {
            var todaysDate = DateTime.UtcNow;
            var midnightToday = new DateTime(todaysDate.Year, todaysDate.Month, todaysDate.Day, 0, 0, 0);

            var gstAccumulator = new GstAccumulator(0.1m, 0.12m, midnightToday);
            gstAccumulator.Accumulate(100, midnightToday.AddDays(-1));
            gstAccumulator.Accumulate(200, midnightToday.AddDays(1));

            gstAccumulator.Total.Should().Be(334.00m);
            gstAccumulator.TotalGst.Should().Be(34.00m);
        }
    }
}

﻿using System;

namespace business.shared.Messages
{
    /// <summary>
    /// This message is used by multiple services to trigger the ProcessAndSendReport operation
    /// </summary>
    public class ProcessGSTMessage
    {

        public string CustomerID { get; set; }
        public DateTime FromDateLocal { get; set; }
        public int NumberMonths { get; set; }
    }
}

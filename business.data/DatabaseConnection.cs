﻿using business.data.Contracts;
using Dapper;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace business.data
{
    public class DatabaseConnection : IDatabaseConnection
    {
        private SqlConnection _databaseConnection;

        public DatabaseConnection()
        {
            var databaseConnectionString = ConfigurationManager.AppSettings["DatabaseConnection"];
            _databaseConnection = new SqlConnection(databaseConnectionString);
        }

        public void Dispose()
        {
            _databaseConnection.Dispose();
        }

        public Task<IEnumerable<T>> QueryAsync<T>(string commandText, object parameters)
        {
            // No need for await here since it's the only async method
            return _databaseConnection.QueryAsync<T>(commandText, parameters);
        }
    }
}

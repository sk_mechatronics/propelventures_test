﻿using business.data.Contracts;
using business.data.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace business.data
{
    public class PurchaseRepository : IPurchaseRepository
    {
        private readonly IDatabaseConnection _databaseConnection;

        public PurchaseRepository(IDatabaseConnection databaseConnection)
        {
            _databaseConnection = databaseConnection;
        }

        public Task<IEnumerable<Purchase>> GetPurchasesAsync(string customerId, DateTime fromDate, DateTime toDate)
        {
            var invoices = new List<Purchase>();
            var commandText = $@"SELECT pr.ItemValue, p.PurchaseDateUTC
					FROM PurchaseRows pr 
                    JOIN Purchase p ON p.Id = pr.PurchaseId 
                    WHERE p.CustomerId = @CustomerId 
                        AND p.PurchaseDateUTC >= @FromDate
                        AND p.PurchaseDateUTC < @ToDate";

            return _databaseConnection.QueryAsync<Purchase>(commandText, 
                new { CustomerId = customerId,
                    FromDate = fromDate.ToString("yyyy-MM-dd HH:mm:ss"),
                    ToDate = toDate.ToString("yyyy-MM-dd HH:mm:ss") });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace business.data.Contracts
{
    public interface IDatabaseConnection : IDisposable
    {
        Task<IEnumerable<T>> QueryAsync<T>(string commandText, object parameters);
    }
}

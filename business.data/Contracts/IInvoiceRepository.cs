﻿using business.data.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace business.data.Contracts
{
    public interface IInvoiceRepository
    {
        Task<IEnumerable<Invoice>> GetInvoicesAsync(string customerId, DateTime fromDate, DateTime toDate);
    }
}
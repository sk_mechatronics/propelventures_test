﻿using business.data.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace business.data.Contracts
{
    public interface IPurchaseRepository
    {
        Task<IEnumerable<Purchase>> GetPurchasesAsync(string customerId, DateTime fromDate, DateTime toDate);
    }
}

﻿using business.data.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace business.data.Contracts
{
    public class InvoiceRepository : IInvoiceRepository
    {
        private readonly IDatabaseConnection _databaseConnection;

        public InvoiceRepository(IDatabaseConnection databaseConnection)
        {
            _databaseConnection = databaseConnection;
        }

        public Task<IEnumerable<Invoice>> GetInvoicesAsync(string customerId, DateTime fromDate, DateTime toDate)
        {
            var invoices = new List<Invoice>();
            var commandText = $@"SELECT ir.ItemPrice, i.InvoiceDateUtc
					FROM InvoiceRows ir 
                    JOIN Invoice i ON i.Id = ir.InvoiceId 
                    WHERE i.CustomerId = @CustomerId 
                        AND i.InvoiceDateUTC >= @FromDate 
                        AND i.InvoiceDateUTC < @ToDate";

            return _databaseConnection.QueryAsync<Invoice>(commandText, 
                new
                    {
                        CustomerId = customerId,
                        FromDate = fromDate.ToString("yyyy-MM-dd HH:mm:ss"),
                        ToDate = toDate.ToString("yyyy-MM-dd HH:mm:ss")
                    });
        }

    }
}

﻿using System;

namespace business.data.Entities
{
    public class Invoice
    {
        public decimal ItemPrice { get; set; }

        public DateTime InvoiceDateUTC { get; set; }
    }
}
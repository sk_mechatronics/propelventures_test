﻿using System;

namespace business.data.Entities
{
    public class Purchase
    {
        public decimal ItemValue { get; set; }

        public DateTime PurchaseDateUTC { get; set; }
    }
}
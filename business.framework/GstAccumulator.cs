﻿using System;

namespace business.framework
{
    public class GstAccumulator
    {
        private readonly decimal _originalGstAmount;
        private readonly decimal _newGstAmount;
        private readonly DateTime? _newGstDateUtc;

        private decimal _total;

        private decimal _totalGst;

        public GstAccumulator(decimal originalGstAmount, decimal newGstAmount, DateTime? newGstDateUtc)
        {
            _originalGstAmount = originalGstAmount;
            _newGstAmount = newGstAmount;
            _newGstDateUtc = newGstDateUtc;
        }

        public void Accumulate(decimal itemPrice, DateTime itemRecordedTime)
        {
            // This will be false if the new gst date has not yet been set.
            var gstAmount = itemRecordedTime >= _newGstDateUtc ? _newGstAmount : _originalGstAmount;

            _total += itemPrice * (1 + gstAmount);
            _totalGst += itemPrice * gstAmount;
        }

        public decimal Total => Math.Round(_total, 2);

        public decimal TotalGst => Math.Round(_totalGst, 2);
    }
}

﻿using business.api;
using business.data.Contracts;
using System;
using System.Threading.Tasks;

namespace business.framework
{
    public sealed class ProcessGST
    {
        private readonly IPropelClient _apiClient;
        private readonly IPurchaseRepository _purchaseRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IGstAccumulatorFactory _gstAccumulatorFactory;

        public ProcessGST(IPropelClient apiClient, 
                            IPurchaseRepository purchaseRepository, 
                            IInvoiceRepository invoiceRepository,
                            IGstAccumulatorFactory gstAccumulatorFactory)
        {
            _apiClient = apiClient;
            _purchaseRepository = purchaseRepository;
            _invoiceRepository = invoiceRepository;
            _gstAccumulatorFactory = gstAccumulatorFactory;
        }

        public async Task<bool> PrepareAndSendReport(string customerId, DateTime fromDateLocal, int numMonths)
        {
            try
            {
                var fromDateUtc = fromDateLocal.ToUniversalTime();
                var toDateUtc = fromDateUtc.AddMonths(numMonths);

                var purchaseGst = await GetPurchaseTotals(customerId, fromDateUtc, toDateUtc);
                var invoiceGst = await GetInvoiceTotals(customerId, fromDateUtc, toDateUtc);


                await _apiClient.SendData(customerId,
                                          purchaseGst.Total,
                                          purchaseGst.TotalGst,
                                          invoiceGst.Total,
                                          invoiceGst.TotalGst);
            }
            catch
            {
                return false;
            }

            return true;
        }

        private async Task<GstAccumulator> GetInvoiceTotals(string customerId, DateTime fromDateUtc, DateTime toDateUtc)
        {
            var invoices = await _invoiceRepository.GetInvoicesAsync(customerId, fromDateUtc, toDateUtc);
            var invoiceGstAccumulator = _gstAccumulatorFactory.CreateGstAccumulator();

            foreach (var invoice in invoices)
            {
                invoiceGstAccumulator.Accumulate(invoice.ItemPrice, invoice.InvoiceDateUTC);
            }

            return invoiceGstAccumulator;
        }

        private async Task<GstAccumulator> GetPurchaseTotals(string customerId, DateTime fromDateUtc, DateTime toDateUtc)
        {
            var purchases = await _purchaseRepository.GetPurchasesAsync(customerId, fromDateUtc, toDateUtc);
            var purchaseGstAccumulator = _gstAccumulatorFactory.CreateGstAccumulator();

            foreach (var purchase in purchases)
            {
                purchaseGstAccumulator.Accumulate(purchase.ItemValue, purchase.PurchaseDateUTC);
            }

            return purchaseGstAccumulator;
        }

        private static decimal RoundCurrency(decimal value)
        {
            return Math.Round(value, 2);
        }
    }
}

﻿namespace business.framework
{
    public interface IGstAccumulatorFactory
    {
        GstAccumulator CreateGstAccumulator();
    }
}

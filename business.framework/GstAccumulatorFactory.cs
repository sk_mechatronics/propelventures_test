﻿using System;
using System.Configuration;

namespace business.framework
{
    public class GstAccumulatorFactory : IGstAccumulatorFactory
    {
        private decimal _originalGstAmount;
        private decimal _newGstAmount;
        private DateTime? _newGstDateUtc;

        public GstAccumulatorFactory()
        {
            _originalGstAmount = decimal.Parse(ConfigurationManager.AppSettings["OriginalGstAmount"]);
            _newGstAmount = decimal.Parse(ConfigurationManager.AppSettings["NewGstAmount"]);
            _newGstDateUtc = GetNewGstEffectiveDateUtc();
        }

        public GstAccumulator CreateGstAccumulator()
        {
            return new GstAccumulator(_originalGstAmount, _newGstAmount, _newGstDateUtc);
        }

        private static DateTime? GetNewGstEffectiveDateUtc()
        {
            // This new date is not known yet.
            var succeeded = DateTime.TryParse(
                ConfigurationManager.AppSettings["NewGstEffectiveDateUtc"], out var rawGstDateUtc);

            if (!succeeded)
            {
                return null;
            }

            if (rawGstDateUtc < DateTime.UtcNow)
            {
                throw new ArgumentException("Cannot retroactively apply GST. Previous reports will be incorrect");
            }

            return rawGstDateUtc;
        }
    }
}
